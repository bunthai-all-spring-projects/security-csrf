package com.example.demo.application;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    @GetMapping("/user")
    public String getHome(
    ) {
        return "user";
    }

    @PostMapping("/userpost")
    public String postHome(){
        return "userpost";
    }

}
