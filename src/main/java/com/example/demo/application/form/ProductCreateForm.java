package com.example.demo.application.form;

import lombok.Value;

@Value
public class ProductCreateForm {
    int id;
    String name;
}
